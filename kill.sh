#!/bin/bash

ps aux | grep python | grep -v grep | awk '{print $2}' | while read -r pid; do
	kill -9 "$pid"
done

