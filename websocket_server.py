#!/usr/bin/env python3

import argparse
import asyncio
import json
import os
import signal
import ssl
import time
from websockets import serve  # type: ignore
from websockets.server import WebSocketServerProtocol  # type: ignore
import subprocess
import sys


CONTINUE_RUNNING = True
VERBOSE = False
SSL_ENABLED = False
JSON_FILE = ""


def sig_handler(signum, frame):
    global CONTINUE_RUNNING
    CONTINUE_RUNNING = False


def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-i", "--interface", type=str, default="localhost", help="Interface address to receive messages onto")
    parser.add_argument("-p", "--port", type=int, default=4435, help="Port to receive onto")
    parser.add_argument("-v", "--verbose", type=bool, default=False, help="Print everything received")
    parser.add_argument("-s", "--ssl", action="store_true", help="Activate SSL, cert.pem and key.pem should be located in the ssl subdirectory")
    parser.add_argument("-f", "--file", type=str, default="", help="Read the json-formatted file and send content of the \"data\" attribute (an array) as separate frames, and 30Hz")
    return parser.parse_known_args()[0]


async def ws_loop(ws_server: WebSocketServerProtocol, path: str):
    print(f"Connection established from {ws_server.remote_address[0]}:{ws_server.remote_address[1]}")

    if not JSON_FILE:
        while CONTINUE_RUNNING:
            new_data = await(ws_server.recv())
            if VERBOSE:
                print(new_data)
    else:
        with open(JSON_FILE, encoding="utf-8") as file:
            file_as_str = file.read()
            frames = json.loads(file_as_str)
            while CONTINUE_RUNNING:
                for frame in frames["data"]:
                    if not CONTINUE_RUNNING:
                        break
                    frame_as_str = json.dumps(frame)
                    await ws_server.send(frame_as_str)
                    await asyncio.sleep(0.033)  # Around 30Hz, not very precise obviously

    print(f"Connection from {ws_server.remote_address[0]}:{ws_server.remote_address[1]} closed")


async def ws_worker(interface: str, port: int, ssl_enabled: bool):
    if ssl_enabled:
        root_path = os.path.realpath('.')
        cert_abs_path = os.path.join(root_path, "ssl/cert.pem")
        key_abs_path = os.path.join(root_path, "ssl/key.pem")

        ssl_context = ssl.SSLContext(ssl.PROTOCOL_TLS_SERVER)
        ssl_context.load_cert_chain(cert_abs_path, key_abs_path)

        async with serve(ws_loop, interface, port, ssl=ssl_context):
            print(f"Listening for Websocket connections through wss://{interface}:{port}")
            while CONTINUE_RUNNING:
                await asyncio.sleep(0.1)
    else:
        async with serve(ws_loop, interface, port, ssl=None):
            print(f"Listening for Websocket connections through ws://{interface}:{port}")
            while CONTINUE_RUNNING:
                await asyncio.sleep(0.1)


args = parse_args()
VERBOSE = args.verbose
SSL_ENABLED = args.ssl
JSON_FILE = args.file

if __name__ == "__main__":
    signal.signal(signal.SIGTERM, sig_handler)
    signal.signal(signal.SIGINT, sig_handler)

    #p = subprocess.Popen(["/home/metalab_legion/Git/audioposesync/build/audioposesync"], stdout=subprocess.PIPE, bufsize=1, universal_newlines=True)
    asyncio.run(ws_worker(args.interface, args.port, SSL_ENABLED))

    while CONTINUE_RUNNING:
        time.sleep(1)
