# CiÖ - Ganesh Baron Aloir

A basic set up for sending LivePose data to a websocket server. 

## Steps: 

1. Run LivePose with the config **websocket_output.json**. Make sure to change the host IP to the adress of your websocket server.

2. Run the websocket_server.py script. Typical use is as follows: 

    ```bash
    ./websocket_server.py -h
    ./websocket_server.py -i <IP_address> -p 4443 # Connection without encryption
    ```
For now, these steps have been tested on a connection without encryption. Tests for a connection with encryption may be part of a future update, if need be. 

## Using a central node

It is possible to aggregate incoming websocket messages from multiple LivePose instances using a central node configuration. To this effect, one can run an additional LivePose instance using the **central_node.json** configuration, which aggregates incoming messages on the websocket server in the "inputs" section, and outputs the aggregate messages on the websocket server in the "outputs" section. 

Note that the central_node can also output messages as a websocket client, in the case where we'd like to connect the output to a websocket server. 

An example of a system with a remote note and a central mode can be found in the subfolder *configuration_remote_central_nodes*.
